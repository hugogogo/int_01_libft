/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstfind.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: simplonco <marvin@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/23 22:48:47 by simplonco         #+#    #+#             */
/*   Updated: 2022/03/24 15:23:22 by simplonco        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 * find an element in two-way list with the comp function
 * return a pointer to it
 */

#include "libft.h"

t_list	*ft_lstfind(t_list *lst, void *to_find, int (*comp)(void*, void *))
{
	while (lst && (!comp(to_find, lst->content)))
		lst = lst->next;
	return (lst);
}
