
#include "libft.h"

long	ft_atol(const char *str)
{
	long long	nbr;
	int			i;
	int			negatif;

	i = 0;
	negatif = 1;
	nbr = 0;
	while ((str[i] == ' ') || (str[i] > 8 && str[i] < 14))
		i++;
	if (str[i] == '-')
		negatif = -1;
	if (str[i] == '+' || str[i] == '-')
		i++;
	while (str[i] >= '0' && str[i] <= '9')
		nbr = nbr * 10 + (str[i++] - '0');
	return (nbr * negatif);
}
