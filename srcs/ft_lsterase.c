/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lsterase.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: simplonco <marvin@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/24 15:31:16 by simplonco         #+#    #+#             */
/*   Updated: 2022/03/24 20:54:20 by simplonco        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 * erase an element in two-way list
 * and rejoin the list
 */

#include "libft.h"

void	ft_lsterase(t_list *lst, void (*del)(void *))
{
	if (!lst || !del)
		return ;
	del(lst->content);
	if (lst->prev)
		lst->prev->next = lst->next;
	if (lst->next)
		lst->next->prev = lst->prev;
	free(lst);
	lst = NULL;
}

