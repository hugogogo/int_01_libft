#include "libftest.h"

char	**create_tab(void)
{
	char	**tab;

	tab = (char **)malloc(sizeof(char *) * (6 + 1));
	tab[0] = strdup("premiere ligne de test");
	tab[1] = strdup("deuxieme ligne de test");
	tab[2] = strdup("troisieme ligne pour le fun");
	tab[3] = strdup("quatrieme linge avec une erreur");
	tab[4] = strdup("cinquieme peche a la ligne");
	tab[5] = strdup("sixieme et derniere ligne");
	return (tab);
}

void	free_tab(char **tab)
{
	free(tab[0]);
	free(tab[1]);
	free(tab[2]);
	free(tab[3]);
	free(tab[4]);
	free(tab[5]);
	free(tab);
}

int	contain_a(char *s)
{
	int	i;

	i = -1;
	while (s[++i])
		if (s[i] == 'a')
			return (1);
	return (0);
}

int	contain_y(char *s)
{
	int	i;

	i = -1;
	while (s[++i])
		if (s[i] == 'y')
			return (1);
	return (0);
}

int	contain_z(char *s)
{
	int	i;

	i = -1;
	while (s[++i])
		if (s[i] == 'z')
			return (1);
	return (0);
}

int	contain_x(char *s)
{
	int	i;

	i = -1;
	while (s[++i])
		if (s[i] == 'x')
			return (1);
	return (0);
}

int	contain_u(char *s)
{
	int	i;

	i = -1;
	while (s[++i])
		if (s[i] == 'u')
			return (1);
	return (0);
}

int	contain_1(char *s)
{
	int	i;

	i = -1;
	while (s[++i])
		if (s[i] == '1')
			return (1);
	return (0);
}

void	compare_any(char **tab, int (*f)(char *), char *s, int solution)
{
	int	i;

	i = ft_any(tab, f);
	if (i != solution)
	{
		write(1, "\033[91m", 5);
		printf("error: ");
		write(1, "\033[0m", 4);
		printf("ft_any(tab, %s) returned", s);
		printf(" %i ", i);
		printf("in tab :\n");
		printf("      %s:\n", tab[0]);
		printf("      %s:\n", tab[1]);
		printf("      %s:\n", tab[2]);
		printf("      %s:\n", tab[3]);
		printf("      %s:\n", tab[4]);
		printf("      %s:\n", tab[5]);
	}
}

void	test_any(void)
{
	char	**tab;

	tab = create_tab();

	compare_any(tab, contain_a, "contain_a", 1);
	compare_any(tab, contain_y, "contain_y", 0);
	compare_any(tab, contain_z, "contain_z", 0);
	compare_any(tab, contain_x, "contain_x", 1);
	compare_any(tab, contain_u, "contain_u", 1);
	compare_any(tab, contain_1, "contain_1", 0);

	free_tab(tab);
}

