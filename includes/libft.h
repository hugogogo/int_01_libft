/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hulamy <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/25 14:45:53 by hulamy            #+#    #+#             */
/*   Updated: 2022/04/21 19:31:18 by simplonco        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <unistd.h>	// for write
# include <stdlib.h>	// for malloc and free
# include "ft_gnl.h"
# include "ft_printf.h"

void				*ft_memset(void *b, int c, size_t len);
void				ft_bzero(void *s, size_t n);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				*ft_memccpy(void *dst, const void *src, int c, size_t n);
void				*ft_memmove(void *dst, const void *src, size_t len);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
size_t				ft_strlen(const char *str);
int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isnumber(char *nb);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_toupper(int c);
int					ft_tolower(int c);
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
char				*ft_strchrset(const char *s, const char *set);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
size_t				ft_strlcpy(char *dst, const char *src, size_t size);
size_t				ft_strlcat(char *dst, const char *src, size_t size);
char				*ft_strnstr(const char *b, const char *l, size_t s);
int					ft_atoi(const char *str);
long				ft_atol(const char *str);
void				*ft_calloc(size_t count, size_t size);
char				*ft_strdup(const char *s1);

char				*ft_substr(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strtrim(char const *s1, char const *set);
char				**ft_split(char const *s, char c);
char				*ft_itoa(long int n);
char				*ft_utoa(unsigned long int n);
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));

void				ft_putchar(char c);
void				ft_putstr(const char *s);
void				ft_putnbr(int n);
void				ft_putnbrbase(int nbr, const char *base);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(const char *s, int fd);
void				ft_putnbr_fd(int n, int fd);
void				ft_putnbrbase_fd(int nbr, const char *base, int fd);

/*
void				ft_putchar(char c);
void				ft_putstr(char *s);
void				ft_putnbr(int n);
void				ft_putnbrbase(int nbr, char *base);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char *s, int fd);
void				ft_putnbr_fd(int n, int fd);
void				ft_putnbrbase_fd(int nbr, char *base, int fd);
*/

/*
void				ft_putendl(char const *str);
void				ft_putendl_fd(char *s, int fd);
*/

typedef struct		s_list
{
	void			*content;
	struct s_list	*prev;
	struct s_list	*next;
}					t_list;

/*
t_list				*ft_lstnew(void *content);
void				ft_lstadd_front(t_list **alst, t_list *n);
void				*ft_lstadd_back(t_list **alst, t_list *n);
int					ft_lstsize(t_list *lst);
t_list				*ft_lstlast(t_list *lst);
void				ft_lstdelone(t_list *lst, void (*del)(void *));
void				ft_lstclear(t_list **lst, void (*del)(void *));
void				ft_lstiter(t_list *lst, void (*f)(void *));
t_list				*ft_lstmap(t_list *l, void *(*f)(void*), void (*d)(void*));
*/
//void				ft_lstremove(t_list *lst, void (*del)(void *));
//t_list			*ft_lstfind(t_list *lst, void *to_find, int (*comp)(void*, void *));
t_list				*ft_lstcreate(void *content);
void				*ft_lstpush_back(t_list **lst, t_list *new);
void				ft_lstpush_front(t_list **alst, t_list *new);
void				ft_lstloop(t_list *lst, void (*f)(void *));
void				ft_lstloop_back(t_list *lst, void (*f)(void *));
t_list				*ft_lstbegin(t_list *lst);
t_list				*ft_lstend(t_list *lst);
t_list				*ft_lstfind(t_list *lst, void *to_find, int (*comp)(void*, void *));
void				ft_lstinsert(t_list *lst, t_list *new);
void				ft_lsterase(t_list *lst, void (*del)(void *));
void				ft_lstfree(t_list *lst, void (*del)(void *));
int					ft_lstlen(t_list *lst);
t_list				*ft_lstcopy(t_list *lst, void *(*cpy)(void *));

char				*ft_strcat(char *s1, const char *s2);
int					ft_strcmp(const char *s1, const char *s2);
char				*ft_strcpy(char *dst, const char *src);
char				*ft_strncat(char *s1, const char *s2, size_t n);
char				*ft_strncpy(char *dst, const char *src, size_t len);
char				*ft_strstr(const char *big, const char *little);
void				ft_strclr(char *s);
void				ft_strdel(char **as);
int					ft_strequ(char const *s1, char const *s2);
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));
char				*ft_strjoinfree(char *s1, char *s2);
char				*ft_strmap(char const *s, char (*f)(char));
int					ft_strnequ(char const *s1, char const *s2, size_t n);
char				*ft_strnew(size_t size);
void				*ft_memalloc(size_t size);
void				ft_memdel(void **ap);
int					ft_atoibase(char *str, char *base);
char				*ft_convertbase(char *nbr, char *base_from, char *base_to);
char				*ft_convertbase_free(char *nbr, char *b_from, char *b_to);
char				**ft_strmultisplit(char *str, char *charset);
int					ft_any(char **tab, int (*f)(char*));
void				ft_foreach(int *tab, int length, void (*f)(int));
int					ft_issort(int *tab, int length, int (*f)(int, int));
int					*ft_arraymap(int *tab, int length, int(*f)(int));
void				ft_putnbrendl(int n);
void				ft_putnbrendl_fd(int n, int fd);
char				*ft_concat_free(char *str1, char *str2);
int					ft_abs(int n);
int					ft_greater(int a, int b);
int					ft_smaller(int a, int b);
int					ft_sign(int i);
int					ft_sqrt(int i);
void				ft_free_tab(char **tab);

int					ft_arrint(int * intarr, int comp, size_t size);

#endif
