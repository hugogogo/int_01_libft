/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstbegin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: simplonco <marvin@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/24 15:05:21 by simplonco         #+#    #+#             */
/*   Updated: 2022/03/24 15:10:45 by simplonco        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 * return a pointer to the first element of a two-way list
 */

#include "libft.h"

t_list	*ft_lstbegin(t_list *lst)
{
	while (lst->prev)
		lst = lst->prev;
	return (lst);
}

