/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstloop.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: simplonco <marvin@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/24 14:37:15 by simplonco         #+#    #+#             */
/*   Updated: 2022/03/24 14:38:32 by simplonco        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 * go forward through all elements of a two-way list
 * and apply the function f to each of them
 */

#include "libft.h"

void	ft_lstloop(t_list *lst, void (*f)(void *))
{
	if (!f)
		return ;
	while (lst)
	{
		f(lst->content);
		lst = lst->next;
	}
}

