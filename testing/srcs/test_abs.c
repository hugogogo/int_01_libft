#include "libftest.h"

void	compare_abs(int i, int ans)
{
	if (ft_abs(i) != ans)
	{
		RED	ft_printf("error: "); COLOREND
			ft_printf("ft_abs(%i)", i);
		RED	ft_printf(" gives "); COLOREND
			ft_printf("%i", ft_abs(i));
		RED	ft_printf(" instead of "); COLOREND
			ft_printf("%i\n", ans);
	}
}

void	test_abs(void)
{
	compare_abs(-1, 1);
	compare_abs(0, 0);
	compare_abs(-0, 0);
	compare_abs(-1000, 1000);
	compare_abs(-2147483647, 2147483647);
}
