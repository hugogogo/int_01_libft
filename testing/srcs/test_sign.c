#include "libft.h"

int		ft_sign(int i)
{
	if (i < 0)
		return (-1);
	return (1);
}
