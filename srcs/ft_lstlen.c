/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: simplonco <marvin@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/24 16:20:25 by simplonco         #+#    #+#             */
/*   Updated: 2022/03/24 16:22:24 by simplonco        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 * return the len of the two-way list
 */

#include "libft.h"

int		ft_lstlen(t_list *lst)
{
	int	size;

	size = 0;
	while (lst)
	{
		size++;
		lst = lst->next;
	}
	return (size);
}
