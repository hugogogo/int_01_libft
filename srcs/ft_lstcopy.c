/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstcopy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: simplonco <marvin@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/24 16:26:27 by simplonco         #+#    #+#             */
/*   Updated: 2022/03/24 16:37:39 by simplonco        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 * copy a two-way list with the copy function for the content
 * and return pointer to the begining of the new list
 */

#include "libft.h"

t_list	*ft_lstcopy(t_list *lst, void *(*cpy)(void *))
{
	t_list	*lst_copy;

	if (!lst || !cpy)
		return (NULL);
	lst_copy = NULL;
	while (lst)
	{
		ft_lstpush_back(&lst_copy, ft_lstcreate(cpy(lst->content)));
		lst = lst->next;
	}
	return (ft_lstbegin(lst_copy));
}

