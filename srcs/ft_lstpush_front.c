/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstpush_front.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: simplonco <marvin@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/24 14:25:25 by simplonco         #+#    #+#             */
/*   Updated: 2022/03/24 14:32:14 by simplonco        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 * add an element to the begining of a two-way list
 */

#include "libft.h"

void	ft_lstpush_front(t_list **alst, t_list *new)
{
	new->next = *alst;
	(*alst)->prev = new;
	*alst = new;
}
