#include "libft.h"

/*
** return the square root of nb
** or the integer value of it
*/
int		ft_sqrt(int nb)
{
	int i;

	i = 0;
	while ((i*i) < nb)
		i++;
	return (i);
}
