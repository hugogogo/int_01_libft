#include "libftest.h"

// the functions precedes by # already exists in the system
int main()
{
	test_abs();
	test_any();
	/*
	test_arraymap.c
	test_atoibase.c
	#	test_atoi.c
	#	test_bzero.c
	#	test_calloc.c
	test_concat_free.c
	test_convertbase.c
	test_convertbase_free.c
	test_foreach.c
	test_gnl.c
	test_greater.c
	#	test_isalnum.c
	#	test_isalpha.c
	#	test_isascii.c
	#	test_isdigit.c
	#	test_isprint.c
	test_issort.c
	test_itoa.c
	test_lstadd_back.c
	test_lstadd_front.c
	test_lstclear.c
	test_lstdelone.c
	test_lstiter.c
	test_lstlast.c
	test_lstmap.c
	test_lstnew.c
	test_lstsize.c
	test_memalloc.c
	#	test_memccpy.c
	#	test_memchr.c
	#	test_memcmp.c
	#	test_memcpy.c
	test_memdel.c
	#	test_memmove.c
	#	test_memset.c
	test_printf.c
	test_putchar.c
	test_putchar_fd.c
	test_putendl.c
	test_putendl_fd.c
	test_putnbrbase.c
	test_putnbr.c
	test_putnbrendl.c
	test_putnbrendl_fd.c
	test_putnbr_fd.c
	test_putstr.c
	test_putstr_fd.c
	test_sign.c
	test_smaller.c
	test_split.c
	test_sqrt.c
	test_strcat.c
	#	test_strchr.c
	test_strchrset.c
	test_strclr.c
	test_strcmp.c
	test_strcpy.c
	test_strdel.c
	#	test_strdup.c
	test_strequ.c
	test_striter.c
	test_striteri.c
	test_strjoin.c
	test_strjoinfree.c
	#	test_strlcat.c
	#	test_strlcpy.c
	#	test_strlen.c
	test_strmap.c
	test_strmapi.c
	test_strmultisplit.c
	test_strncat.c
	#	test_strncmp.c
	test_strncpy.c
	test_strnequ.c
	test_strnew.c
	#	test_strnstr.c
	#	test_strrchr.c
	test_strstr.c
	test_strtrim.c
	test_substr.c
	#	test_tolower.c
	#	test_toupper.c
	test_utoa.c
	*/

	return 0;
}
