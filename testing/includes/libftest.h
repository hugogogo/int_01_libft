#ifndef LIBFTEST_H
# define LIBFTEST_H
# include "libft.h"
# include <unistd.h>	// for write
# include <stdio.h>		// for printf
# include <stdlib.h>	// for malloc
# include <string.h>	// for strdup and strlen and strcmp
# include <ctype.h>		// for characters like isalpha or isdigit etc

# define RED write(1, "\033[91m", 5);
# define GREEN write(1, "\033[92m", 5);
# define COLOREND write(1, "\033[0m", 4);

void	test_memset(void);
void	test_bzero(void);
void	test_memcpy(void);
void	test_memccpy(void);
void	test_memmove(void);
void	test_memchr(void);
void	test_memcmp(void);
void	test_strlen(void);
void	test_isalpha(void);
void	test_isdigit(void);
void	test_isalnum(void);
void	test_isascii(void);
void	test_isprint(void);
void	test_toupper(void);
void	test_tolower(void);
void	test_strchr(void);
void	test_strrchr(void);
void	test_strchrset(void);
void	test_strncmp(void);
void	test_strlcpy(void);
void	test_strlcat(void);
void	test_strnstr(void);
void	test_atoi(void);
void	test_calloc(void);
void	test_strdup(void);
void	test_substr(void);
void	test_strjoin(void);
void	test_strtrim(void);
void	test_split(void);
void	test_itoa(void);
void	test_utoa(void);
void	test_strmapi(void);
void	test_putchar_fd(void);
void	test_putstr_fd(void);
void	test_putendl_fd(void);
void	test_putnbr_fd(void);
void	test_lstnew(void);
void	test_lstadd_front(void);
void	test_lstsize(void);
void	test_lstlast(void);
void	test_lstadd_back(void);
void	test_lstdelone(void);
void	test_lstclear(void);
void	test_lstiter(void);
void	test_lstmap(void);
void	test_strcat(void);
void	test_strcmp(void);
void	test_strcpy(void);
void	test_strncat(void);
void	test_strncpy(void);
void	test_strstr(void);
void	test_strjoinfree(void);
void	test_strclr(void);
void	test_strdel(void);
void	test_strequ(void);
void	test_striter(void);
void	test_striteri(void);
void	test_strmap(void);
void	test_strnequ(void);
void	test_strnew(void);
void	test_memalloc(void);
void	test_memdel(void);
void	test_putchar(void);
void	test_putendl(void);
void	test_putnbr(void);
void	test_putnbrendl(void);
void	test_putnbrendl_fd(void);
void	test_putnbrbase(void);
void	test_putstr(void);
void	test_any(void);
void	test_atoibase(void);
void	test_convertbase(void);
void	test_convertbase_free(void);
void	test_foreach(void);
void	test_issort(void);
void	test_arraymap(void);
void	test_strmultisplit(void);
void	test_gnl(void);
void	test_concat_free(void);
void	test_printf(void);
void	test_next_word(void);
void	test_convert(void);
void	test_flag_transform(void);
void	test_flag_transform_bonus(void);
void	test_abs(void);
void	test_greater(void);
void	test_smaller(void);
void	test_sign(void);
void	test_sqrt(void);

#endif
