/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstcreate.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: simplonco <marvin@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/24 13:12:50 by simplonco         #+#    #+#             */
/*   Updated: 2022/03/24 14:29:04 by simplonco        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 * create a new two-way list
 */

#include "libft.h"

t_list	*ft_lstcreate(void *content)
{
	t_list		*lst;

	lst = (t_list *)malloc(sizeof(*lst));
	if (!lst)
		return (NULL);
	lst->content = content;
	lst->prev = NULL;
	lst->next = NULL;
	return (lst);
}

