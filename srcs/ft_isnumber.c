
#include "libft.h"

int	ft_isnumber(char *nb)
{
	int i;

	i = 0;
	if (nb[i] == '+' || nb[i] == '-')
		i++;
	if (nb[i] == '\0')
		return (0);
	while (nb[i] != '\0')
	{
		if (ft_isdigit(nb[i]) == 0)
		return (0);
		i++;
	}
	return (1);
}

