/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstpush_back.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: simplonco <marvin@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/24 13:37:11 by simplonco         #+#    #+#             */
/*   Updated: 2022/03/24 14:28:49 by simplonco        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 *	add an element to the end of a two-way list, or first if list has no element
 *  return NULL if element is NULL (eg it returned from ft_lstcreate and failed)
 *         or else address to the element added
 */

#include "libft.h"

void	*ft_lstpush_back(t_list **lst, t_list *new)
{
	t_list	*tmp;

	if (!new)
		return (NULL);
	if (lst)
	{
		tmp = *lst;
		if (!tmp)
			*lst = new;
		else
		{
			while (tmp->next)
				tmp = tmp->next;
			tmp->next = new;
			new->prev = tmp;
		}
	}
	return (new);
}
