#include "libft.h"

static int	check(const char *base)
{
	int i;
	int j;

	i = 0;
	while (base[i])
	{
		j = i + 1;
		while (base[j])
		{
			if (base[i] == base[j])
				return (0);
			j++;
		}
		if (base[i] == '-' || base[i] == '+')
			return (0);
		i++;
	}
	if (i >= 2)
		return (1);
	return (0);
}

void		ft_putnbrbase_fd(int nbr, const char *base, int fd)
{
	int		i;
	long	n;

	i = 0;
	n = nbr;
	if (check(base))
	{
		if (n < 0)
		{
			ft_putchar_fd('-', fd);
			n = -n;
		}
		while (base[i])
			i++;
		if (n >= i)
			ft_putnbrbase_fd(n / i, base, fd);
		ft_putchar_fd(base[n % i], fd);
	}
}

