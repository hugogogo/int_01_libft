/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstloop_back.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: simplonco <marvin@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/24 15:01:37 by simplonco         #+#    #+#             */
/*   Updated: 2022/03/24 15:02:02 by simplonco        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 * go backward through all elements of a two-way list
 * and apply the function f to each of them
 */

#include "libft.h"

void	ft_lstloop_back(t_list *lst, void (*f)(void *))
{
	if (!f)
		return ;
	while (lst)
	{
		f(lst->content);
		lst = lst->prev;
	}
}

