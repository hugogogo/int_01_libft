/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstinsert.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: simplonco <marvin@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/24 15:22:46 by simplonco         #+#    #+#             */
/*   Updated: 2022/03/24 15:31:52 by simplonco        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 * insert an element in two-way list just after the one in parameter
 * and rejoin the list
 */

#include "libft.h"

void	ft_lstinsert(t_list *lst, t_list *new)
{
	new->next = lst->next;
	lst->next = new;
	new->next->prev = new;
	new->prev = lst;
}

