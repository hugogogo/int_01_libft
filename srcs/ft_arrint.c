/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrint.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: simplonco <marvin@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/21 19:30:25 by simplonco         #+#    #+#             */
/*   Updated: 2022/04/21 19:30:35 by simplonco        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 * search through an arr of int if it contains the int comp
 * return 0 if found, 1 otherwise
 */

#include "libft.h"

int	ft_arrint(int * intarr, int comp, size_t size)
{
	size_t	i;

	if (!intarr)
		return 1;
	i = -1;
	while (++i < size)
		if (intarr[i] == comp)
			return 0;
	return 1;
}
