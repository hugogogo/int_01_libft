/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstfree.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: simplonco <marvin@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/24 15:49:35 by simplonco         #+#    #+#             */
/*   Updated: 2022/03/24 16:16:47 by simplonco        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 * delete and free an element of a two-way list and all the followings
 */

#include "libft.h"

void	ft_lstfree(t_list *lst, void (*del)(void *))
{
	t_list	*next;

	if (lst && lst->prev)
		lst->prev->next = NULL;
	while (lst != NULL)
	{
		next = lst->next;
		del(lst->content);
		free(lst);
		lst = next;
	}
}

