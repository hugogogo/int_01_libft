/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstend.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: simplonco <marvin@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/24 15:11:41 by simplonco         #+#    #+#             */
/*   Updated: 2022/03/24 15:12:17 by simplonco        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 * return a pointer to the last element of a two-way list
 */

#include "libft.h"

t_list	*ft_lstend(t_list *lst)
{
	while (lst->next)
		lst = lst->next;
	return (lst);
}
